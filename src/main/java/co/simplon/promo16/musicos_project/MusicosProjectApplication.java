package co.simplon.promo16.musicos_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicosProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MusicosProjectApplication.class, args);
	}

}
