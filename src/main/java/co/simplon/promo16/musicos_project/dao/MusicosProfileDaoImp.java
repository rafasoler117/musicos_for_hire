package co.simplon.promo16.musicos_project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.musicos_project.entity.MusicosProfile;

@Repository
public class MusicosProfileDaoImp implements MusicosProfileDAO, UserDetailsService {

    @Autowired
    DataSource datasource;
    Connection cnx;

    private static final String Show_All_Musicos = "SELECT * FROM user_musicos";
    private static final String Show_Musicos = "SELECT * FROM user_musicos WHERE user_id = ?";
    private static final String Create_Musicos_Profile = 
                    "INSERT INTO user_musicos (firstName, lastName, région, email, role, password) VALUES (?, ?, ?, ?, ? ,?)";
    private static final String Update_Musicos_Profile = 
                    "UPDATE user_musicos SET firstName = ?, lastName = ?, région = ?, email = ?, role = ?, password = ? WHERE user_id = ?";
    private static final String Delete_Musicos_Profile = "DELETE FROM user_musicos WHERE user_id = ?";
    private static final String Find_By_email = "SELECT * FROM user_musicos WHERE email = ?";
    private static final String Find_All_Musicos_InPresta = 
    "SELECT * FROM user_musicos LEFT JOIN presta_musicos ON user_musicos.user_id=presta_musicos.user_id WHERE presta_musicos.presta_id=?";
    private static final String Find_All_Musicos_ByInstru =
    "SELECT * FROM user_musicos LEFT JOIN instrument_musicos ON user_musicos.user_id=instrument_musicos.user_id WHERE instrument_musicos.instrument_id=?";
    private static final String Find_All_Musicos_ByStyle = 
    "SELECT * FROM user_musicos LEFT JOIN style_musicos ON user_musicos.user_id=style_musicos.user_id WHERE style_musicos.style_id=?";
    private static final String Add_Instru_ToMusicos = "INSERT INTO musicos_instrument (user_id,instrument_id) VALUES (?,?)";
    private static final String Add_Style_ToMusicos = "INSERT INTO musicos_style (user_id,style_id) VALUES (?,?)";
    private static final String Add_presta_ToMusicos = "INSERT INTO musicos_presta (user_id,presta_id) VALUES (?,?)";
    private static final String Update_About = "UPDATE user_musicos SET about=? WHERE user_id = ?";
    @Override
    public List<MusicosProfile> findall() {
        
        List<MusicosProfile> musicProfileList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Show_All_Musicos);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                musicProfileList.add(new MusicosProfile
                (result.getInt("user_id"),
                result.getString("firstName"),
                  result.getString("lastName"),
                   result.getString("région"),
                    result.getString("email"),
                    result.getString("role"),
                    result.getString("about")));
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return musicProfileList;
    }

    @Override
    public MusicosProfile find(Integer id) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Show_Musicos);
            ResultSet result = stmt.executeQuery();
            if(result.next()){
                MusicosProfile musicos = new MusicosProfile
                (result.getInt("user_id"),
                result.getString("firstName"), 
                result.getString("lastName"), 
                result.getString("région"),
                result.getString("email"),
                result.getString("role"),
                result.getString("about"));

                return musicos;
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return null;
    }

    @Override
    public boolean create(MusicosProfile musicos) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement((Create_Musicos_Profile),PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, musicos.getFirstName());
            stmt.setString(2, musicos.getLastName());
            stmt.setString(3, musicos.getRégion());
            stmt.setString(4, musicos.getEmail());
            stmt.setString(5, musicos.getRole());
            stmt.setString(6, musicos.getPassword());
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                musicos.setProfile_id(result.getInt(1));

                return true;
            }


        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return false;
    }

    @Override
    public void update(MusicosProfile musicos) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Update_Musicos_Profile);
            stmt.setString(1, musicos.getFirstName());
            stmt.setString(2, musicos.getLastName());
            stmt.setString(3, musicos.getRégion());
            stmt.setString(4, musicos.getEmail());
            stmt.setString(5, musicos.getRole());
            stmt.setString(6, musicos.getPassword());
            stmt.setInt(7, musicos.getProfile_id());
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }

    @Override
    public void remove(Integer id) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Delete_Musicos_Profile);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }

    @Override
    public MusicosProfile findByEmail(String email){
        
        MusicosProfile musicos_User;
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_By_email);
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                musicos_User = new MusicosProfile(result.getInt("user_id"),
                result.getString("firstName"),
                result.getString("lastName"),
                result.getString("région"),
                result.getString("email"),
                result.getString("role"),
                result.getString("password"),
                result.getString("about"));
              
            return musicos_User;
          }
            cnx.close();
        } catch (Exception e) {

            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return null;
        
    }

    @Override
    public List<MusicosProfile> findAllMusicosInPresta(Integer prestaId) {
        List<MusicosProfile> musicosList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_All_Musicos_InPresta);
            stmt.setInt(1, prestaId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                musicosList.add(new MusicosProfile(result.getInt("user_id"),
                                                    result.getString("firstName"),
                                                    result.getString("lastName"),
                                                    result.getString("région"),
                                                    result.getString("email"),
                                                    result.getString("role"),
                                                    result.getString("about")));
                
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }

        return musicosList;
    }
    @Override
    public List<MusicosProfile> findAllMusicosByInstru(Integer instruId) {
        List<MusicosProfile> musicosList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_All_Musicos_ByInstru);
            stmt.setInt(1, instruId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                musicosList.add(new MusicosProfile(result.getInt("user_id"),
                                                    result.getString("firstName"),
                                                    result.getString("lastName"),
                                                    result.getString("région"),
                                                    result.getString("email"),
                                                    result.getString("role"),
                                                    result.getString("about")));
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return musicosList;
    }

    @Override
    public List<MusicosProfile> findAllMusicosByStyle(Integer styleId) {
        List<MusicosProfile> musicosList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_All_Musicos_ByStyle);
            stmt.setInt(1, styleId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                musicosList.add(new MusicosProfile(result.getInt("user_id"),
                                                    result.getString("firstName"),
                                                    result.getString("lastName"),
                                                    result.getString("région"),
                                                    result.getString("email"),
                                                    result.getString("role"),
                                                    result.getString("about")));
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return musicosList;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MusicosProfile musicos_user = findByEmail(username);
        if(musicos_user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return musicos_user;

    }

    @Override
    public void addInstruToMusicos(Integer instruId, Integer musicosId) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Add_Instru_ToMusicos);
            stmt.setInt(1, musicosId);
            stmt.setInt(2, instruId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }

    @Override
    public void addStyleToMusicos(Integer styleId, Integer musicosId) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Add_Style_ToMusicos);
            stmt.setInt(1, musicosId);
            stmt.setInt(2, styleId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }

    @Override
    public void addPrestaToMusicos(Integer prestaId, Integer musicosId) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Add_presta_ToMusicos);
            stmt.setInt(1, musicosId);
            stmt.setInt(2, prestaId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }

    @Override
    public void updateAbout(MusicosProfile musicos) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Update_About);
            stmt.setString(1, musicos.getAbout());
            stmt.setInt(2, musicos.getProfile_id());
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }  
}
