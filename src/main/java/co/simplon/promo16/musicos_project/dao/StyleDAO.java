package co.simplon.promo16.musicos_project.dao;

import java.util.List;

import co.simplon.promo16.musicos_project.entity.Style;

public interface StyleDAO {
    
    List<Style> findAllStyle();
    Style findStyle(Integer id);
    boolean createStyle(Style style);
    void updateStyle(Style style);
    void deleteStyle(Integer id);
    List<Style> findAllStyleOfMusicos(Integer musicosId);
    List<Style> findAllStyleOfPresta(Integer prestaId);
    void createStyleInPresta(Style style, Integer prestaId);
    void createStyleInMusicos(Style style, Integer musicosId);
}
