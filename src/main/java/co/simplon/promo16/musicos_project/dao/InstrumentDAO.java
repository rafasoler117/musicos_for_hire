package co.simplon.promo16.musicos_project.dao;

import java.util.List;

import co.simplon.promo16.musicos_project.entity.Instrument;

public interface InstrumentDAO {
    
    List<Instrument> findAllInstru();
    Instrument findInstru(Integer id);
    boolean createinstru(Instrument instru);
    boolean updateInstru(Instrument instru);
    boolean deleteInstru(Integer id);
    List<Instrument> findAllInstruOfMusicos(Integer musicosId);
    List<Instrument> findAllInstruOfPresta(Integer prestaId);
}
