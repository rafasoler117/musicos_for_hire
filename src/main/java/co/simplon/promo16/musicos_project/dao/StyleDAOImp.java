package co.simplon.promo16.musicos_project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.musicos_project.entity.Style;

@Repository
public class StyleDAOImp implements StyleDAO {

    @Autowired
    DataSource datasource;
    Connection cnx;
    private static final String Show_All_Style = "SELECT * FROM style";
    private static final String Show_Style_ById = "SELECT * FROM style WHERE style_id = ?";
    private static final String Create_Style = "INSERT INTO style (style) VALUES (?)";
    private static final String Update_Style = "UPDATE style SET style = ? WHERE style_id = ?";
    private static final String Delete_Style = "DELETE FROM style WHERE style_id = ?";
    private static final String Find_All_Style_OfMusicos = 
    "SELECT * FROM style LEFT JOIN musicos_style ON style.style_id=musicos_style.style_id WHERE musicos_style.user_id=?";
    private static final String Find_All_Style_OfPresta =
    "SELECT * FROM style LEFT JOIN presta_style ON style.style_id=presta_style.style_id WHERE presta_style.presta_id=?";
    
    @Override
    public List<Style> findAllStyle() {
        
        List<Style> styleList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Show_All_Style);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                styleList.add(new Style(result.getInt("style_id"), result.getString("style")));
            }
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }

        return styleList;
    }

    @Override
    public Style findStyle(Integer id) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Show_Style_ById);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if(result.next()){
                Style style = new Style( result.getString("style"));
                return style;
            }
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return null;
    }

    @Override
    public boolean createStyle(Style style) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement((Create_Style),PreparedStatement.RETURN_GENERATED_KEYS);;
            stmt.setString(1, style.getStyle());
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                style.setStyle_id(result.getInt(1));

                return true;
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return false;
    }

    @Override
    public void updateStyle(Style style) {
        

        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Update_Style);
            stmt.setString(1, style.getStyle());
            stmt.setInt(2, style.getStyle_id());
            stmt.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        
    }

    @Override
    public void deleteStyle(Integer id) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Delete_Style);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }

    @Override
    public List<Style> findAllStyleOfMusicos(Integer musicosId) {
        List<Style> styleList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_All_Style_OfMusicos);
            stmt.setInt(1, musicosId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                styleList.add(new Style(result.getInt("style_id"),result.getString("style")));
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return styleList;
    }

    @Override
    public List<Style> findAllStyleOfPresta(Integer prestaId) {
        List<Style> styleList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_All_Style_OfPresta);
            stmt.setInt(1, prestaId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                styleList.add(new Style(result.getInt("style_id"),result.getString("style")));
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return styleList;
    }

    @Override
    public void createStyleInPresta(Style style, Integer prestaId) {
        
        
    }

    @Override
    public void createStyleInMusicos(Style style, Integer musicosId) {
        // TODO Auto-generated method stub
        
    }
    public DataSource getDatasource() {
        return datasource;
    }
    
    public void setDatasource(DataSource datasource) {
        this.datasource = datasource;
    }
}
