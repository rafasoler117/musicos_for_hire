package co.simplon.promo16.musicos_project.dao;

import java.util.List;

import co.simplon.promo16.musicos_project.entity.MusicosProfile;

public interface MusicosProfileDAO {
    
    List<MusicosProfile> findall();
    MusicosProfile find(Integer id);
    boolean create(MusicosProfile musicos);
    void update(MusicosProfile musicos);
    void remove(Integer id);
    MusicosProfile findByEmail(String email);
    List<MusicosProfile> findAllMusicosInPresta(Integer prestaId);
    List<MusicosProfile> findAllMusicosByInstru(Integer instruId);
    List<MusicosProfile> findAllMusicosByStyle(Integer styleId);
    void addInstruToMusicos(Integer instruId, Integer musicosId);
    void addStyleToMusicos(Integer styleId, Integer musicosId);
    void addPrestaToMusicos(Integer prestaId, Integer musicosId);
    void updateAbout(MusicosProfile musicos);
}

