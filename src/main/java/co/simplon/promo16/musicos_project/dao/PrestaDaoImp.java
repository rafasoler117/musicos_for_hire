package co.simplon.promo16.musicos_project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.musicos_project.entity.Presta;

@Repository
public class PrestaDaoImp implements PrestaDAO{
    

    @Autowired
    DataSource datasource;
    Connection cnx;
    @Autowired
    MusicosProfileDAO musicosProfile;
    @Autowired
    InstrumentDAO instru;
    @Autowired
    StyleDAO style;
    
    private static final String Show_All_Presta = "SELECT * FROM presta";
    private static final String Get_Presta = "SELECT * FROM presta WHERE presta_id = ?";
    private static final String Add_Presta = "INSERT INTO presta (date_presta, lieu, about) VALUES (?,?,?)";
    private static final String Delete_Presta = "DELETE FROM presta WHERE presta_id = ?";
    private static final String Delete_Instru_InPresta = "DELETE FROM presta_instrument WHERE presta_id = ?";
    private static final String Delete_Style_InPresta = "DELETE FROM presta_style WHERE presta_id = ?";
    private static final String Update_Presta = 
                            "UPDATE presta SET date_presta = ?, lieu = ? WHERE presta_id = ?";
    private static final String Create_Style_inPresta = "INSERT INTO presta_style (presta_id,style_id) VALUES (?,?)";
    private static final String Create_Instu_InPresta = "INSERT INTO presta_instrument (presta_id,instrument_id) VALUES (?,?)";
    private static final String Create_Musicos_InPresta = "INSERT INTO presta_musicos (presta_id,user_id) VALUES (?,?)";
    

    @Override
    public List<Presta> findallPresta() {
        List<Presta> prestaList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt1 = cnx.prepareStatement(Show_All_Presta);
            ResultSet result1 = stmt1.executeQuery();
            while(result1.next()){
                Presta presta = new Presta(
                    result1.getInt("presta_id"),
                    result1.getString("date_presta"),
                    result1.getString("lieu"),
                    result1.getString("about")
                );
               
                prestaList.add(presta);
                presta.setMusicos_profile(musicosProfile.findAllMusicosInPresta(result1.getInt("presta_id")));
                presta.setInstrument(instru.findAllInstruOfPresta(result1.getInt("presta_id")));
                presta.setStyle(style.findAllStyleOfPresta(result1.getInt("presta_id")));
              
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return prestaList;
    }
    @Override
    public Presta findByPrestaID(Integer prestaId) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Get_Presta);
            stmt.setInt(1, prestaId);
            ResultSet result = stmt.executeQuery();
            if(result.next()){
            
                Presta presta = new Presta(result.getInt("presta_id"), result.getString("date_presta"), result.getString("lieu"), result.getString("about"));
                
                presta.setMusicos_profile(musicosProfile.findAllMusicosInPresta(result.getInt("presta_id")));
                presta.setInstrument(instru.findAllInstruOfPresta(result.getInt("presta_id")));
                presta.setStyle(style.findAllStyleOfPresta(result.getInt("presta_id")));

                return presta;
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
      return null;
   }
    @Override
    public boolean savePresta(Presta presta) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement((Add_Presta),PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, presta.getDate_presta());
            stmt.setString(2, presta.getLieu());
            stmt.setString(3, presta.getAbout());
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                presta.setPresta_id(result.getInt(1));

                return true;
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return false;
    }
    @Override
    public void updatePresta(Presta presta) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Update_Presta);
            stmt.setString(1, presta.getDate_presta());
            stmt.setString(2, presta.getLieu());
            stmt.setInt(3, presta.getPresta_id());
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }
    @Override
    public boolean removePresta(Integer id) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Delete_Presta);
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return false;
    }
    @Override
    public void removeInstruFromPresta(Integer prestaID){
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Delete_Instru_InPresta);
            stmt.setInt(1, prestaID);
           
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }
    @Override
    public void removeStyleFromPresta(Integer prestaID){
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Delete_Style_InPresta);
            stmt.setInt(1, prestaID);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
    }

    @Override
    public void addInstruToPresta(Integer instruId, Integer prestaId) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Create_Instu_InPresta);
            stmt.setInt(1, prestaId);
            stmt.setInt(2, instruId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }  
    }
    @Override
    public void addStyleToPresta(Integer styleId, Integer prestaId) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Create_Style_inPresta);
            stmt.setInt(1, prestaId);
            stmt.setInt(2, styleId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }   
    }
    @Override
    public void addMusicosToPresta(Integer musicosId, Integer prestaId) {
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Create_Musicos_InPresta);
            stmt.setInt(1, prestaId);
            stmt.setInt(2, musicosId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }   
    }
   
    @Override
    public List<Presta> findAllPrestaByInstru(Integer instruId) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public List<Presta> findAllPrestaByStyle(Integer styleId) {
        // TODO Auto-generated method stub
        return null;
    }
    public DataSource getDatasource() {
        return datasource;
    }
    
    public void setDatasource(DataSource datasource) {
        this.datasource = datasource;
    }
    @Override
    public List<Presta> findPrestaBySearch(String search) {
        List<Presta> list = new ArrayList<>();
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("SELECT * FROM presta WHERE about LIKE ?");
            stmt.setString(1, "%"+search+"%");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Presta(
                                result.getInt("presta_id"),
                                result.getString("date_presta"),
                                result.getString("lieu"),
                                result.getString("about"))
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return list;
    }

}
