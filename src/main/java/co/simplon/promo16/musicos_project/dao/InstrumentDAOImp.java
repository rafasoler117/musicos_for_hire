package co.simplon.promo16.musicos_project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.musicos_project.entity.Instrument;

@Repository
public class InstrumentDAOImp implements InstrumentDAO {

    
    @Autowired
    DataSource datasource;
    Connection cnx;
    
    private static final String Show_All_Instru = "SELECT * FROM instrument";
    private static final String Show_Instru_ById = "SELECT * FROM instrument WHERE instrument_id=?";
    private static final String Create_Instru = "INSERT INTO instrument (instrument) VALUES (?)";
    private static final String Update_Instru = "UPDATE instrument SET instrument = ? WHERE instrument_id = ?";
    private static final String Delete_Instru = "DELETE FROM instrument WHERE instrument_id = ?";
    private static final String Find_All_Instru_OfMusicos =
    "SELECT * FROM instrument LEFT JOIN musicos_instrument ON instrument.instrument_id=musicos_instrument.instrument_id WHERE musicos_instrument.user_id=?";
    private static final String Find_All_Instru_OfPresta = 
    "SELECT * FROM instrument LEFT JOIN presta_instrument ON instrument.instrument_id=presta_instrument.instrument_id WHERE presta_instrument.presta_id=?";
    
    @Override
    public List<Instrument> findAllInstru() {
        
        List<Instrument> instruList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Show_All_Instru);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                instruList.add(new Instrument(result.getInt("instrument_id"),result.getString("instrument")));
            }
            cnx.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return instruList;
    }

    @Override
    public Instrument findInstru(Integer id) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Show_Instru_ById);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if(result.next()){
                Instrument instru = new Instrument(result.getString("instrument"));

                return instru;
            }
            cnx.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return null;
    }

    @Override
    public boolean createinstru(Instrument instru) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement((Create_Instru),PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, instru.getInstrument());
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                instru.setInstrument_id(result.getInt(1));

                return true;
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return false;
    }

    @Override
    public boolean updateInstru(Instrument instru) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Update_Instru);
            stmt.setString(1, instru.getInstrument());
            stmt.setInt(2, instru.getInstrument_id());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return false;
    }

    @Override
    public boolean deleteInstru(Integer instrument_id) {
        
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Delete_Instru);
            stmt.setInt(1, instrument_id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return false;
    }

    @Override
    public List<Instrument> findAllInstruOfMusicos(Integer musicosId) {
        List<Instrument> instruList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_All_Instru_OfMusicos);
            stmt.setInt(1, musicosId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                instruList.add(new Instrument(result.getInt("instrument_id"),result.getString("instrument")));
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return instruList;
    }

    @Override
    public List<Instrument> findAllInstruOfPresta(Integer prestaId) {
        List<Instrument> instruList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(Find_All_Instru_OfPresta);
            stmt.setInt(1, prestaId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                instruList.add(new Instrument(result.getInt("instrument_id"),result.getString("instrument")));
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return instruList;
    }


public DataSource getDatasource() {
    return datasource;
}

public void setDatasource(DataSource datasource) {
    this.datasource = datasource;
}
}
