package co.simplon.promo16.musicos_project.dao;

import java.util.List;

import co.simplon.promo16.musicos_project.entity.Presta;

public interface PrestaDAO {
    
    List<Presta> findallPresta();
    Presta findByPrestaID(Integer prestaId);
    boolean savePresta(Presta presta);
    void updatePresta(Presta presta);
    boolean removePresta(Integer id);
    void removeInstruFromPresta(Integer prestaID);
    void removeStyleFromPresta(Integer prestaID);
    List<Presta> findAllPrestaByInstru(Integer instruId);
    List<Presta> findAllPrestaByStyle(Integer styleId);
    void addInstruToPresta(Integer instruId, Integer prestaId);
    void addStyleToPresta(Integer styleId, Integer prestaId);
    void addMusicosToPresta(Integer musicosId, Integer prestaId);
    List<Presta> findPrestaBySearch(String search);
}
