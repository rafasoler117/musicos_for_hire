package co.simplon.promo16.musicos_project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo16.musicos_project.dao.PrestaDAO;
import co.simplon.promo16.musicos_project.entity.Presta;


@RestController
@RequestMapping("/api/person")
public class RestControllerJS {
    
    @Autowired
    PrestaDAO prestaDao;

    
    @GetMapping
    public List<Presta> listPresta(@RequestParam(required = false)String search){
        if(search == null){
            return prestaDao.findallPresta();
        }
        return prestaDao.findPrestaBySearch(search);
    }
}
