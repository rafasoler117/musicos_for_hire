package co.simplon.promo16.musicos_project.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import co.simplon.promo16.musicos_project.dao.InstrumentDAO;
import co.simplon.promo16.musicos_project.dao.MusicosProfileDAO;
import co.simplon.promo16.musicos_project.dao.PrestaDAO;
import co.simplon.promo16.musicos_project.dao.StyleDAO;
import co.simplon.promo16.musicos_project.entity.Instrument;
import co.simplon.promo16.musicos_project.entity.MusicosProfile;
import co.simplon.promo16.musicos_project.entity.Presta;
import co.simplon.promo16.musicos_project.entity.Style;

@Controller
public class PrestaController {
    
    @Autowired
    MusicosProfileDAO musicosDao;
    @Autowired
    InstrumentDAO instruDao;
    @Autowired
    StyleDAO styleDao;
    @Autowired
    PrestaDAO prestaDao;

    @GetMapping("/presta-list")
    public String showPrestaList(Model model){
        model.addAttribute("prestas", prestaDao.findallPresta());
        

        return"presta-list";
    }

    @GetMapping("/create-presta/")
    public String createPresta(Model model){
        model.addAttribute("musicos", musicosDao.findall());
        model.addAttribute("instrument", instruDao.findAllInstru());
        model.addAttribute("style", styleDao.findAllStyle());
        List<Integer> instru = new ArrayList<>();
        List<Style> style = new ArrayList<>();
        List<MusicosProfile> musicos = new ArrayList<>();

        Presta presta = new Presta();
        presta.setInstruID(instru);
        presta.setStyle(style);
        presta.setMusicos_profile(musicos);
        model.addAttribute("presta", presta);
        return"create-presta";
    }

    @PostMapping("/create-presta/")
    public String formPresta(Presta presta){
        prestaDao.savePresta(presta);
        for (Instrument instru  : presta.getInstrument()) {
            int instruId = Integer.parseInt(instru.getInstrument());
            prestaDao.addInstruToPresta(instruId, presta.getPresta_id());
        }
        for (Style  style : presta.getStyle()) {
            int styleId = Integer.parseInt(style.getStyle());
            prestaDao.addStyleToPresta(styleId, presta.getPresta_id());
        }   
        return "redirect:/presta-list";
    }

    @GetMapping("/delete/{id}")
    public String deletePresta(@PathVariable("id") Integer id){
        prestaDao.removeInstruFromPresta(id);
        prestaDao.removeStyleFromPresta(id);
        prestaDao.removePresta(id);
        
        return "redirect:/presta-list";
    }
    @GetMapping
    public List<Presta> listPresta(@RequestParam(required = false)String search){
        if(search == null){
            return prestaDao.findallPresta();
        }
        return prestaDao.findPrestaBySearch(search);
    }
    @GetMapping("/presta-card/{id}")
    public String showPresta(@PathVariable("id") Integer id,Model model){
       
        model.addAttribute("prestas", prestaDao.findByPrestaID(id));
        
        return"presta-card";
    }
}
