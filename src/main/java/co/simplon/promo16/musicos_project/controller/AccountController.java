package co.simplon.promo16.musicos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.musicos_project.dao.InstrumentDAO;
import co.simplon.promo16.musicos_project.dao.MusicosProfileDAO;
import co.simplon.promo16.musicos_project.dao.StyleDAO;
import co.simplon.promo16.musicos_project.entity.MusicosProfile;

@Controller
public class AccountController {

    @Autowired
    MusicosProfileDAO musicosDao;
    @Autowired
    InstrumentDAO instruDao;
    @Autowired
    StyleDAO styleDao;
    
    
    @GetMapping("/account")
    public String showAccount(Authentication authentication, Model model){
        MusicosProfile musicos = (MusicosProfile)authentication.getPrincipal();
        model.addAttribute("musicos", musicos);
        model.addAttribute("MusicosProfile", musicosDao.findall());
        model.addAttribute("instrument", instruDao.findAllInstru());
        model.addAttribute("instruMusicos", instruDao.findAllInstruOfMusicos(musicos.getProfile_id()));
        model.addAttribute("style", styleDao.findAllStyle());
        model.addAttribute("styleMusicos", styleDao.findAllStyleOfMusicos(musicos.getProfile_id()));
        return"account";
    }


    @GetMapping("/add-instru/{instrument}")
    public String addinstru(@PathVariable ("instrument") Integer instruId, Authentication authentication) {
        MusicosProfile musicos = (MusicosProfile)authentication.getPrincipal();
        musicosDao.addInstruToMusicos(instruId, musicos.getProfile_id());
        return "redirect:/account";
    }

    @GetMapping("/add-style/{style}")
    public String addStyle(@PathVariable("style")Integer styleId,Authentication authentication){
        MusicosProfile musicos = (MusicosProfile)authentication.getPrincipal();
        musicosDao.addStyleToMusicos(styleId, musicos.getProfile_id());
        return "redirect:/account";
    }

    @PostMapping("/add-about/")
    public String addAbout(Authentication authentication,@ModelAttribute("about") String about){
        MusicosProfile musicos = (MusicosProfile)authentication.getPrincipal();
       musicos.setAbout(about);
        musicosDao.updateAbout(musicos);
        return "redirect:/account";
    }
        
}
