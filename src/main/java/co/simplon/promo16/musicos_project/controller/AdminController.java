package co.simplon.promo16.musicos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import co.simplon.promo16.musicos_project.dao.MusicosProfileDAO;
import co.simplon.promo16.musicos_project.entity.MusicosProfile;

@Controller
@RequestMapping("/admin")
public class AdminController {
    
    @Autowired
    MusicosProfileDAO musicosRepo;
    
    @GetMapping("/manage-users") // Donc ça on y accède sur http://localhost:8080/admin/manage-users
    public String manageUser(Model model) {
        model.addAttribute("user", musicosRepo.findall());
        return "manage-users";
    }    
    
}
