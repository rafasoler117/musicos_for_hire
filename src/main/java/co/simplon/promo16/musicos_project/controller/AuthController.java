package co.simplon.promo16.musicos_project.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.musicos_project.dao.MusicosProfileDaoImp;
import co.simplon.promo16.musicos_project.entity.MusicosProfile;

@Controller
public class AuthController {
    
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private MusicosProfileDaoImp musicosRepo;

    @GetMapping("/")
    public String home(){
        return "index";
    }

    @GetMapping("/register")
    public String showRegister(Model model) {
        
        
        model.addAttribute("musicosProfile", new MusicosProfile());

        return "register";
    }

    @PostMapping("/register")
    public String addUsers(@Valid MusicosProfile musicosProfile,BindingResult result, Model model){
        if(result.hasErrors()){
       
            return"register";
        }
    
        if(musicosRepo.findByEmail(musicosProfile.getEmail())!= null){
            model.addAttribute("feedback", "Users already exists");

            return "register";
        }
        String hashpass = encoder.encode(musicosProfile.getPassword());
            musicosProfile.setPassword(hashpass);
            musicosProfile.setRole("ROLE_USER");
            musicosRepo.create(musicosProfile);
        return "redirect:/account";
    }
}
