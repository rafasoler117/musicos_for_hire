package co.simplon.promo16.musicos_project.entity;

import java.util.Collection;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class MusicosProfile implements UserDetails {
    
    private Integer profile_id;
    @NotBlank@Size(min= 4, max=50)@NotEmpty
    private String firstName;
    @NotBlank@Size(min= 4, max=50)@NotEmpty
    private String lastName;
    @NotBlank@Size(min= 4, max=50)@NotEmpty
    private String région;
    @NotBlank@Email@NotEmpty@Size(min= 4, max=50)
    private String email;
    private String role;
    @NotBlank@Size(min= 4, max=50)@NotEmpty
    private String password;
    private String about;
    
  
    public MusicosProfile(Integer profile_id, @NotBlank @Size(min = 4, max = 50) @NotEmpty String firstName,
            @NotBlank @Size(min = 4, max = 50) @NotEmpty String lastName,
            @NotBlank @Size(min = 4, max = 50) @NotEmpty String région,
            @NotBlank @Email @NotEmpty @Size(min = 4, max = 50) String email, String role, String about) {
        this.profile_id = profile_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.région = région;
        this.email = email;
        this.role = role;
        this.about = about;
    }
    public MusicosProfile(String firstName,String lastName,String région, String email, String role, String about) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.région = région;
        this.email = email;
        this.role = role;
        this.about = about;
    }
    public MusicosProfile(Integer profile_id, @NotBlank @Size(min = 4, max = 50) @NotEmpty String firstName,
            @NotBlank @Size(min = 4, max = 50) @NotEmpty String lastName,
            @NotBlank @Size(min = 4, max = 50) @NotEmpty String région,
            @NotBlank @Email @NotEmpty @Size(min = 4, max = 50) String email, String role,
            @NotBlank @Size(min = 4, max = 50) @NotEmpty String password, String about) {
        this.profile_id = profile_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.région = région;
        this.email = email;
        this.role = role;
        this.password = password;
        this.about = about;
    }
    public String getAbout() {
        return about;
    }
    public void setAbout(String about) {
        this.about = about;
    }
  
    public MusicosProfile() {
    }
    public Integer getProfile_id() {
        return profile_id;
    }
    public void setProfile_id(Integer profile_id) {
        this.profile_id = profile_id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getRégion() {
        return région;
    }
    public void setRégion(String région) {
        this.région = région;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        
        return List.of(new SimpleGrantedAuthority(role));
    }
    @Override
    public String getUsername() {
        
        return email;
    }
    @Override
    public boolean isAccountNonExpired() {
        
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {

        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }
    @Override
    public boolean isEnabled() {

        return true;
    }
    
    
    
    
    
    
}
