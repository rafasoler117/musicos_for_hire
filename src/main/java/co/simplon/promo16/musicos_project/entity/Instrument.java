package co.simplon.promo16.musicos_project.entity;

public class Instrument {
    
    private Integer instrument_id;
    private String instrument;
    
     
    public Instrument(Integer instrument_id, String instrument) {
        this.instrument_id = instrument_id;
        this.instrument = instrument;
    }
    public Instrument(String instrument) {
        this.instrument = instrument;
    }
    public Instrument() {
    }
    public Integer getInstrument_id() {
        return instrument_id;
    }
    public void setInstrument_id(Integer instrument_id) {
        this.instrument_id = instrument_id;
    }
    public String getInstrument() {
        return instrument;
    }
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }
    
    
}
