package co.simplon.promo16.musicos_project.entity;

public class Style {
    
    private Integer style_id;
    private String style;
    
    
    public Style(Integer style_id, String style) {
        this.style_id = style_id;
        this.style = style;
    }
    public Style(String style) {
        this.style = style;
    }
    public Style() {
    }
    public Integer getStyle_id() {
        return style_id;
    }
    public void setStyle_id(Integer style_id) {
        this.style_id = style_id;
    }
    public String getStyle() {
        return style;
    }
    public void setStyle(String style) {
        this.style = style;
    }

}
