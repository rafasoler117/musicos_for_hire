package co.simplon.promo16.musicos_project.entity;

import java.util.List;

public class Presta {
    
    private Integer presta_id;
    private String date_presta;
    private String lieu;
    private List<MusicosProfile> musicos_profile;
    private List<Instrument> instrument;
    private List<Style> style;
    private String about;
    private List<Integer> instruID;
    
  
    public Presta(Integer presta_id, String date_presta, String lieu, String about) {
        this.presta_id = presta_id;
        this.date_presta = date_presta;
        this.lieu = lieu;
        this.about = about;
    }
    public List<Integer> getInstruID() {
        return instruID;
    }
    public void setInstruID(List<Integer> instruID) {
        this.instruID = instruID;
    }
    public Presta(Integer presta_id, String date_presta, String lieu, List<MusicosProfile> musicos_profile,
            List<Instrument> instrument, List<Style> style) {
        this.presta_id = presta_id;
        this.date_presta = date_presta;
        this.lieu = lieu;
        this.musicos_profile = musicos_profile;
        this.instrument = instrument;
        this.style = style;
    }
    public String getAbout() {
        return about;
    }
    public void setAbout(String about) {
        this.about = about;
    }
    public Presta(String date_presta, String lieu) {
        this.date_presta = date_presta;
        this.lieu = lieu;
        
    }
    public Presta() {
    }
    public Integer getPresta_id() {
        return presta_id;
    }
    public void setPresta_id(Integer presta_id) {
        this.presta_id = presta_id;
    }
    public String getDate_presta() {
        return date_presta;
    }
    public void setDate_presta(String date_presta) {
        this.date_presta = date_presta;
    }
    public String getLieu() {
        return lieu;
    }
    public void setLieu(String lieu) {
        this.lieu = lieu;
    }
    public List<MusicosProfile> getMusicos_profile() {
        return musicos_profile;
    }
    public void setMusicos_profile(List<MusicosProfile> musicos_profile) {
        this.musicos_profile = musicos_profile;
    }
    public List<Instrument> getInstrument() {
        return instrument;
    }
    public void setInstrument(List<Instrument> instrument) {
        this.instrument = instrument;
    }
    public List<Style> getStyle() {
        return style;
    }
    public void setStyle(List<Style> style) {
        this.style = style;
    }
    @Override
    public String toString() {
        return "Presta [instrument=" + instrument + ", musicos_profile=" + musicos_profile + ", style=" + style + "]";
    }
    
}
