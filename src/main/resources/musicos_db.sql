DROP TABLE IF EXISTS style_presta;
DROP TABLE IF EXISTS style_musicos;
DROP TABLE IF EXISTS instrument_presta;
DROP TABLE IF EXISTS instrument_musicos;
DROP TABLE IF EXISTS musicos_presta;
DROP TABLE IF EXISTS musicos_style;
DROP TABLE IF EXISTS musicos_instrument;
DROP TABLE IF EXISTS presta_style;
DROP TABLE IF EXISTS presta_instrument;
DROP TABLE IF EXISTS presta_musicos;
DROP TABLE IF EXISTS presta;
DROP TABLE IF EXISTS style;
DROP TABLE IF EXISTS instrument;
DROP TABLE IF EXISTS user_musicos;
CREATE TABLE IF NOT EXISTS user_musicos(
    user_id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    région VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL,
    password VARCHAR(255)NOT NULL,
    about TEXT    
);

CREATE TABLE IF NOT EXISTS instrument(
    instrument_id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
    instrument VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS style(
    style_id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
    style VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS presta(
    presta_id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
    date_presta VARCHAR(255) NOT NULL,
    lieu VARCHAR(255) NOT NULL,
    about TEXT
    
);

CREATE TABLE IF NOT EXISTS presta_musicos(
    presta_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    PRIMARY KEY(presta_id,user_id),
    FOREIGN KEY (presta_id) REFERENCES presta(presta_id),
    FOREIGN KEY (user_id) REFERENCES user_musicos(user_id)
);

CREATE TABLE IF NOT EXISTS presta_instrument(
    presta_id INTEGER NOT NULL,
    instrument_id INTEGER NOT NULL,
    PRIMARY KEY(presta_id,instrument_id),
    FOREIGN KEY(presta_id) REFERENCES presta(presta_id),
    FOREIGN KEY(instrument_id) REFERENCES instrument(instrument_id)
);

CREATE TABLE IF NOT EXISTS presta_style(
    presta_id INTEGER NOT NULL,
    style_id INTEGER NOT NULL,
    PRIMARY KEY(presta_id,style_id),
    FOREIGN KEY(presta_id) REFERENCES presta(presta_id),
    FOREIGN KEY(style_id) REFERENCES style(style_id)
);

CREATE TABLE IF NOT EXISTS musicos_instrument(
    user_id INTEGER NOT NULL,
    instrument_id INTEGER NOT NULL,
    PRIMARY KEY(user_id,instrument_id),
    FOREIGN KEY(user_id) REFERENCES user_musicos(user_id),
    FOREIGN KEY(instrument_id) REFERENCES instrument(instrument_id)
);

CREATE TABLE IF NOT EXISTS musicos_style(
    user_id INTEGER NOT NULL,
    style_id INTEGER NOT NULL,
    PRIMARY KEY(user_id,style_id),
    FOREIGN KEY(user_id) REFERENCES user_musicos(user_id),
    FOREIGN KEY(style_id) REFERENCES style(style_id)
);

CREATE TABLE IF NOT EXISTS musicos_presta(
    user_id INTEGER NOT NULL,
    presta_id INTEGER NOT NULL,
    PRIMARY KEY(user_id,presta_id),
    FOREIGN KEY(user_id) REFERENCES user_musicos(user_id),
    FOREIGN KEY(presta_id) REFERENCES presta(presta_id)
);

CREATE TABLE IF NOT EXISTS instrument_musicos(
    instrument_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    PRIMARY KEY(instrument_id,user_id),
    FOREIGN KEY(instrument_id) REFERENCES instrument(instrument_id),
    FOREIGN KEY(user_id) REFERENCES user_musicos(user_id)
);

CREATE TABLE IF NOT EXISTS instrument_presta(
    instrument_id INTEGER NOT NULL,
    presta_id INTEGER NOT NULL,
    PRIMARY KEY(instrument_id,presta_id),
    FOREIGN KEY(instrument_id) REFERENCES instrument(instrument_id),
    FOREIGN KEY(presta_id) REFERENCES presta(presta_id)
);

CREATE TABLE IF NOT EXISTS style_musicos(
    style_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    PRIMARY KEY(style_id,user_id),
    FOREIGN KEY(style_id) REFERENCES style(style_id),
    FOREIGN KEY(user_id) REFERENCES user_musicos(user_id)
);

CREATE TABLE IF NOT EXISTS style_presta(
    style_id INTEGER NOT NULL,
    presta_id INTEGER NOT NULL,
    PRIMARY KEY(style_id,presta_id),
    FOREIGN KEY(style_id) REFERENCES style(style_id),
    FOREIGN KEY(presta_id) REFERENCES presta(presta_id)
);

INSERT INTO user_musicos(firstName,lastName,région,email,role,password) VALUES ('raf','sol','osef','test@test.com','ADMIN',1234);

INSERT INTO presta(date_presta,lieu) VALUES ('10/10/10','osef');
INSERT INTO presta(date_presta,lieu) VALUES ('10/10/10','osef');
INSERT INTO instrument (instrument) VALUES ('guitare');

INSERT INTO musicos_instrument (user_id,instrument_id) VALUES (1,1);

INSERT INTO presta_instrument (presta_id,instrument_id) VALUES (1,1);
INSERT INTO style (style) VALUES ('Rock'),('Jazz'),('Electro'),('Pop'),('Metal'),('Funk'),('Soul'),('Rap');

INSERT INTO presta_style VALUES (1,1),(1,2);
