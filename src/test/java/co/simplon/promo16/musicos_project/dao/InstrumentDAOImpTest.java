package co.simplon.promo16.musicos_project.dao;


import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import co.simplon.promo16.musicos_project.entity.Instrument;


public class InstrumentDAOImpTest {

    InstrumentDAOImp trackRepository;
    
        @BeforeEach
        void init() {
            trackRepository = new InstrumentDAOImp();
            DataSource dataSource = new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .addScript("musicos_db.sql")
                    .build();
            trackRepository.setDatasource(dataSource);
        }
    @Test
    void testCreateinstru() {
        Instrument instru = new Instrument("test");
        assertTrue(trackRepository.createinstru(instru));
        assertNotNull(instru.getInstrument_id());
    }

    @Test
    void testDeleteInstru() {
        
        assertTrue(trackRepository.deleteInstru(1));
    }

    @Test
    void testFindAllInstru() {
       
        assertNotEquals(0, trackRepository.findAllInstru().size());
    }

    @Test
    void testFindAllInstruOfMusicos() {

        assertNotEquals(0, trackRepository.findAllInstruOfMusicos(1).size());
    }

    @Test
    void testFindAllInstruOfPresta() {
        assertNotEquals(0, trackRepository.findAllInstruOfPresta(1).size());
    }

    @Test
    void testFindInstru() {
        assertNotNull(trackRepository.findInstru(1));
    }

    @Test
    void testUpdateInstru() {
        Instrument track = new Instrument("instrument");
            track.setInstrument_id(1);;
            assertTrue(trackRepository.updateInstru(track));
    }
} 
