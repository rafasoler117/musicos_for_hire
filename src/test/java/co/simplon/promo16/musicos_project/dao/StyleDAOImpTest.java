package co.simplon.promo16.musicos_project.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import co.simplon.promo16.musicos_project.entity.Style;

public class StyleDAOImpTest {
    
    StyleDAOImp stylerepo;

    @BeforeEach
        void init() {
            stylerepo = new StyleDAOImp();
            DataSource dataSource = new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .addScript("musicos_db.sql")
                    .build();
            stylerepo.setDatasource(dataSource);
        }

        @Test
        void testCreatePresta() {
            Style style = new Style(1, "style");
            assertTrue(stylerepo.createStyle(style));
            assertNotNull(style.getStyle_id());
        }

        @Test
    void testFindAllInstru() {
       
        assertNotEquals(0, stylerepo.findAllStyle().size());
    }
    @Test
    void testfindallstyleinpresta(){
        assertEquals(2, stylerepo.findAllStyleOfPresta(1).size());
    }
}
