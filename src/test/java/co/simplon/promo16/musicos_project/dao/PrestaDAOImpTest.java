package co.simplon.promo16.musicos_project.dao;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import co.simplon.promo16.musicos_project.entity.Presta;

public class PrestaDAOImpTest {
    
    PrestaDaoImp repo;

    @BeforeEach
        void init() {
            repo = new PrestaDaoImp();
            DataSource dataSource = new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .addScript("musicos_db.sql")
                    .build();
            repo.setDatasource(dataSource);
        }
        @Test
        void testCreatePresta() {
            Presta presta = new Presta("date_presta", "lieu");
            assertTrue(repo.savePresta(presta));
            assertNotNull(presta.getPresta_id());
        }
        
        @Test
        void testDeletepresta() {
            
            assertTrue(repo.removePresta(1));
        }
     
}
